# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""
MindSpore implementation of `MixNet`.
Refer to MixConv: Mixed Depthwise Convolutional Kernels
"""

import mindspore.nn as nn
import mindspore.ops as ops
import mindspore.common.initializer as init

__all__ = [
    "MixNet",
    "mixnet_s",
    "mixnet_m",
    "mixnet_l"
]


def round_filters(filters, multiplier=1.0, divisor=8, min_depth=None):
    if min_depth is None:
        min_depth = divisor
    filters *= multiplier
    new_filters = max(min_depth, int(filters + divisor / 2) // divisor * divisor)
    if new_filters < 0.9 * filters:
        new_filters += divisor
    return new_filters


def activation(activate='relu'):
    if activate.lower() == 'relu':
        return nn.ReLU6()
    elif activate.lower() == 'swish':
        return nn.HSwish()
    else:
        raise NotImplementedError(f"Only ReLU and Swish are used in MixNet, but got {activate}.")


class Swish(nn.Cell):
    def __init__(self):
        super(Swish, self).__init__()
        self.sigmoid = ops.Sigmoid()

    def construct(self, x):
        return x * self.sigmoid(x)


class SqueezeAndExcitation(nn.Cell):
    r"""Squeeze-and-Excitation Block, from
    `"Squeeze-and-Excitation Networks" <https://arxiv.org/abs/1709.01507>`_.
    Global Pooling -> FC -> ReLU -> FC -> Sigmoid -> Scale."""

    def __init__(self, n_channels, squeeze_channels, activate='relu'):
        super(SqueezeAndExcitation, self).__init__()
        self.se_block = nn.SequentialCell([
            nn.Conv2d(n_channels, squeeze_channels, 1, 1, pad_mode="pad", padding=0),
            activation(activate),
            nn.Conv2d(squeeze_channels, n_channels, 1, 1, pad_mode="pad", padding=0),
            nn.Sigmoid(),
        ])
        self.mean = ops.ReduceMean(True)

    def construct(self, x):
        out = self.mean(x, (2, 3))
        out = self.se_block(out)
        return out * x


class MDConv(nn.Cell):
    """Mixed Depth-wise Convolution"""

    def __init__(self, n_channels, kernel_sizes_or_n_groups, stride):
        super(MDConv, self).__init__()
        if isinstance(kernel_sizes_or_n_groups, list):
            self.kernel_sizes = kernel_sizes_or_n_groups
            self.n_groups = len(self.kernel_sizes)
        elif isinstance(kernel_sizes_or_n_groups, int):
            self.n_groups = kernel_sizes_or_n_groups
            self.kernel_sizes = [2 * idx + 3 for idx in range(self.n_groups)]
        else:
            raise NotImplementedError(f"int as n_groups or list as kernel_sizes, "
                                      f"but got{type(kernel_sizes_or_n_groups)}: {kernel_sizes_or_n_groups}.")
        self.split_channels = [n_channels // self.n_groups for _ in range(self.n_groups)]
        self.split_channels[0] += n_channels - sum(self.split_channels)

        self.layers = nn.CellList()
        for i in range(self.n_groups):
            self.layers.append(
                nn.Conv2d(in_channels=self.split_channels[i], out_channels=self.split_channels[i],
                          kernel_size=self.kernel_sizes[i], stride=stride,
                          pad_mode="pad", padding=int(self.kernel_sizes[i] // 2), group=self.split_channels[i])
            )
        self.concat = ops.Concat(axis=1)
        # fixme: The definition of mindspore.ops.operations.split is different from torch.split.
        #  1. `output_num` is the opposite of `split_size(int)`. 2. `split_sections(list)` is not supported.

    def construct(self, x):
        outputs = []
        start, end = 0, 0
        for group_idx in range(self.n_groups):
            start, end = end, end + self.split_channels[group_idx]
            sp_x = x[:, start:end]
            layer = self.layers[group_idx]
            outputs.append(layer(sp_x))
        return self.concat(outputs)


class MixBlock(nn.Cell):
    """Basic Block of MixNet"""

    def __init__(self, in_channels, out_channels, kernel_sizes, stride,
                 expand_ratio, expand_group, project_group, se_ratio, activate):
        super(MixBlock, self).__init__()
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.stride = stride
        self.expand_ratio = expand_ratio
        self.se_ratio = se_ratio
        mid_channels = in_channels * expand_ratio

        if self.expand_ratio != 1:
            self.expand_conv = nn.SequentialCell([
                nn.Conv2d(in_channels, mid_channels, kernel_size=1, stride=1, group=expand_group),
                nn.BatchNorm2d(mid_channels),
                activation(activate)
            ])
        else:
            self.expand_conv = None
        self.md_conv = nn.SequentialCell([
            MDConv(mid_channels, kernel_sizes, stride),
            nn.BatchNorm2d(mid_channels),
            activation(activate)
        ])
        if self.se_ratio > 0:
            squeeze_channels = max(1, int(in_channels * se_ratio))
            self.se = SqueezeAndExcitation(mid_channels, squeeze_channels, activate)
        else:
            self.se = None
        self.project_conv = nn.SequentialCell([
            nn.Conv2d(mid_channels, out_channels, kernel_size=1, stride=1, group=project_group),
            nn.BatchNorm2d(out_channels)
        ])

    def construct(self, x):
        identity = x
        if self.expand_ratio != 1:
            x = self.expand_conv(x)
        x = self.md_conv(x)
        if self.se_ratio > 0:
            x = self.se(x)
        x = self.project_conv(x)

        if self.in_channels == self.out_channels and self.stride == 1:
            x = x + identity
        return x


class MixNet(nn.Cell):
    r"""MixNet model class, based on
    `"MixConv: Mixed Depthwise Convolutional Kernels" <https://arxiv.org/abs/1907.09595>`_

    Args:
        stem_channels (int) : number of stem output channels.
        last_channels (int) : number of last MixBlock output channels.
        head_channels (int) : number of head Conv output channels.
        block_configs (list) : configuration of each MixBlock.
        dropout_rate (float) : rate of dropout for classifier.
        n_classes (int) : number of classification classes.
    """

    def __init__(self, stem_channels, last_channels, head_channels, block_configs, dropout_rate, n_classes=1000):
        super(MixNet, self).__init__()
        self.stem_channels = stem_channels
        self.last_channels = last_channels
        self.head_channels = head_channels
        self.stem = nn.SequentialCell([
            nn.Conv2d(in_channels=3, out_channels=stem_channels, kernel_size=3, stride=2, pad_mode="pad", padding=1),
            nn.BatchNorm2d(stem_channels),
            nn.ReLU6(),  # todo: ReLU or HSwish?
        ])
        layers = []
        for (in_channels, out_channels, kernel_sizes, stride,
             expand_ratio, expand_group, project_group, se_ratio, activate) in block_configs:
            layers.append(MixBlock(
                in_channels, out_channels, kernel_sizes, stride,
                expand_ratio, expand_group, project_group, se_ratio, activate
            ))
        self.features = nn.SequentialCell(layers)
        self.head = nn.SequentialCell([
            nn.Conv2d(last_channels, head_channels, kernel_size=1),
            nn.BatchNorm2d(head_channels),
            nn.ReLU6(),  # todo: ReLU or HSwish?
        ])
        self.global_pool = nn.AvgPool2d(7)
        self.dropout = nn.Dropout(keep_prob=1 - dropout_rate)
        self.classifier = nn.Dense(head_channels, n_classes)
        self.reshape = ops.Reshape()
        self._initialize_weights()

    def construct(self, x):
        x = self.stem(x)
        x = self.features(x)
        x = self.head(x)
        x = self.global_pool(x)
        x = self.reshape(x, (-1, self.head_channels))
        x = self.dropout(x)
        x = self.classifier(x)
        return x

    def _initialize_weights(self):
        for _, m in self.cells_and_names():
            if isinstance(m, nn.Conv2d):
                m.weight.set_data(init.initializer(init.HeNormal(), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))
            elif isinstance(m, nn.BatchNorm2d) or isinstance(m, nn.BatchNorm1d):
                m.gamma.set_data(init.initializer(init.Constant(1), m.gamma.shape))
                if m.beta is not None:
                    m.beta.set_data(init.initializer(init.Constant(0), m.beta.shape))
            elif isinstance(m, nn.Dense):
                m.weight.set_data(init.initializer(init.Constant(1), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))


def mixnet_s(**kwargs):
    # in_channels, out_channels, kernel_sizes, stride, expand_ratio, expand_group, project_group, se_ratio, activate
    block_configs = [
        [16,  16,  [3],              1, 1, 1, 1, 0.0,  'ReLU'],
        [16,  24,  [3],              2, 6, 2, 2, 0.0,  'ReLU'],
        [24,  24,  [3],              1, 3, 2, 2, 0.0,  'ReLU'],

        [24,  40,  [3, 5, 7],        2, 6, 1, 1, 0.5,  'Swish'],
        [40,  40,  [3, 5],           1, 6, 2, 2, 0.5,  'Swish'],
        [40,  40,  [3, 5],           1, 6, 2, 2, 0.5,  'Swish'],
        [40,  40,  [3, 5],           1, 6, 2, 2, 0.5,  'Swish'],

        [40,  80,  [3, 5, 7],        2, 6, 1, 2, 0.25, 'Swish'],  # todo: stride = 1?
        [80,  80,  [3, 5],           1, 6, 1, 2, 0.25, 'Swish'],
        [80,  80,  [3, 5],           1, 6, 1, 2, 0.25, 'Swish'],

        [80,  120, [3, 5, 7],        1, 6, 2, 2, 0.5,  'Swish'],  # todo: stride = 2?
        [120, 120, [3, 5, 7, 9],     1, 3, 2, 2, 0.5,  'Swish'],  # todo: kernel_size = [3, 5, 7]?
        [120, 120, [3, 5, 7, 9],     1, 3, 2, 2, 0.5,  'Swish'],  # todo: kernel_size = [3, 5, 7]?

        [120, 200, [3, 5, 7, 9, 11], 2, 6, 1, 1, 0.5,  'Swish'],
        [200, 200, [3, 5, 7, 9],     1, 6, 1, 2, 0.5,  'Swish'],
        [200, 200, [3, 5, 7, 9],     1, 6, 1, 2, 0.5,  'Swish']
    ]
    stem_channels = 16
    last_channels = 200
    head_channels = 1536
    dropout_rate = 0.2
    return MixNet(stem_channels, last_channels, head_channels, block_configs, dropout_rate, **kwargs)


def mixnet_m(multiplier=1.0, **kwargs):
    block_configs = [
        [24,  24,  [3],          1, 1, 1, 1, 0.0,  'ReLU'],
        [24,  32,  [3, 5, 7],    2, 6, 2, 2, 0.0,  'ReLU'],
        [32,  32,  [3],          1, 3, 2, 2, 0.0,  'ReLU'],

        [32,  40,  [3, 5, 7, 9], 2, 6, 1, 1, 0.5,  'Swish'],
        [40,  40,  [3, 5],       1, 6, 2, 2, 0.5,  'Swish'],
        [40,  40,  [3, 5],       1, 6, 2, 2, 0.5,  'Swish'],
        [40,  40,  [3, 5],       1, 6, 2, 2, 0.5,  'Swish'],

        [40,  80,  [3, 5, 7],    2, 6, 1, 1, 0.25, 'Swish'],  # todo: stride = 1?
        [80,  80,  [3, 5, 7, 9], 1, 6, 2, 2, 0.25, 'Swish'],
        [80,  80,  [3, 5, 7, 9], 1, 6, 2, 2, 0.25, 'Swish'],
        [80,  80,  [3, 5, 7, 9], 1, 6, 2, 2, 0.25, 'Swish'],

        [80,  120, [3],          1, 6, 1, 1, 0.5,  'Swish'],  # todo: stride = 2?
        [120, 120, [3, 5, 7, 9], 1, 3, 2, 2, 0.5,  'Swish'],
        [120, 120, [3, 5, 7, 9], 1, 3, 2, 2, 0.5,  'Swish'],
        [120, 120, [3, 5, 7, 9], 1, 3, 2, 2, 0.5,  'Swish'],

        [120, 200, [3, 5, 7, 9], 2, 6, 1, 1, 0.5,  'Swish'],
        [200, 200, [3, 5, 7, 9], 1, 6, 1, 2, 0.5,  'Swish'],
        [200, 200, [3, 5, 7, 9], 1, 6, 1, 2, 0.5,  'Swish'],
        [200, 200, [3, 5, 7, 9], 1, 6, 1, 2, 0.5,  'Swish']
    ]
    for block in block_configs:
        block[0] = round_filters(block[0], multiplier)
        block[1] = round_filters(block[1], multiplier)
    stem_channels = round_filters(24, multiplier)
    last_channels = round_filters(200, multiplier)
    head_channels = round_filters(1536, multiplier=1.0)
    dropout_rate = 0.25
    return MixNet(stem_channels, last_channels, head_channels, block_configs, dropout_rate, **kwargs)


def mixnet_l(**kwargs):
    return mixnet_m(1.3, **kwargs)


if __name__ == '__main__':
    # fixme: difficult to debug which layer got data of wrong shape!
    import numpy as np
    import mindspore
    from mindspore import Tensor
    from mindspore import context
    context.set_context(mode=mindspore.PYNATIVE_MODE)

    model = mixnet_l()
    print(model)
    dummy_input = Tensor(np.random.rand(8, 3, 224, 224), dtype=mindspore.float32)
    y = model(dummy_input)
    print(y.shape)
