# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""""""

from typing import List

import mindspore.nn as nn
import mindspore.common.initializer as init
from mindspore import Tensor

from .layers.conv_norm_act import Conv2dNormActivation
from .layers.pooling import GlobalAvgPooling
from .utils import make_divisible, load_pretrained
from .registry import register_model

__all__ = [
]


def _cfg(url='', **kwargs):
    return {
        'url': url,
        'num_classes': 1000,
        'first_conv': '', 'classifier': '',
        **kwargs
    }


default_cfgs = {
    'mnasnet0.5': _cfg(url=''),
    'mnasnet0.75': _cfg(url=''),
    'mnasnet1.0': _cfg(url=''),
    'mnasnet1.4': _cfg(url=''),

}


class InvertedResidual(nn.Cell):

    def __init__(self,
                 in_channel: int,
                 out_channel: int,
                 stride: int,
                 kernel_size: int,
                 expand_ratio: int,
                 ) -> None:
        super(InvertedResidual, self).__init__()
        assert stride in [1, 2]

        hidden_dim = round(in_channel * expand_ratio)
        self.use_res_connect = stride == 1 and in_channel == out_channel

        self.layers = nn.SequentialCell([
            # pw
            Conv2dNormActivation(in_channel, hidden_dim, kernel_size=1),
            # dw
            Conv2dNormActivation(
                hidden_dim,
                hidden_dim,
                kernel_size=kernel_size,
                stride=stride,
                groups=hidden_dim
            ),
            # pw-linear
            nn.Conv2d(hidden_dim, out_channel, kernel_size=1,
                      stride=1, has_bias=False),
            nn.BatchNorm2d(out_channel)
        ])

    def construct(self, x: Tensor) -> Tensor:
        if self.use_res_connect:
            return self.layers(x) + x
        return x


class Mnasnet(nn.Cell):
    """Mnasnet"""

    def __init__(self,
                 alpha: float,
                 in_channels: int = 3,
                 num_classes: int = 1000,
                 drop_rate: float = 0.2
                 ):
        super(Mnasnet, self).__init__()

        inverted_residual_setting = [
            # t, c, n, s, k
            [3, 24, 3, 2, 3],  # -> 56x56
            [3, 40, 3, 2, 5],  # -> 28x28
            [6, 80, 3, 2, 5],  # -> 14x14
            [6, 96, 2, 1, 3],  # -> 14x14
            [6, 192, 4, 2, 5],  # -> 7x7
            [6, 320, 1, 1, 3],  # -> 7x7
        ]

        mid_channel = make_divisible(32 * alpha, 8)
        input_channel = make_divisible(16 * alpha, 8)
        last_channel = make_divisible(1280 * max(1.0, alpha), 8)

        features: List[nn.Cell] = [
            Conv2dNormActivation(in_channels, mid_channel, kernel_size=3, stride=2),
            Conv2dNormActivation(mid_channel, mid_channel, kernel_size=3, stride=1, groups=mid_channel),
            Conv2dNormActivation(mid_channel, input_channel, kernel_size=1, stride=1, activation=None)
        ]

        for t, c, n, s, k in inverted_residual_setting:
            output_channel = make_divisible(c * alpha, 8)
            for i in range(n):
                stride = s if i == 0 else 1
                features.append(InvertedResidual(input_channel, output_channel,
                                                 stride=stride, kernel_size=k, expand_ratio=t, ))
                input_channel = output_channel

        features.append(
            Conv2dNormActivation(input_channel, last_channel, kernel_size=1, stride=1)
        )
        self.features = nn.SequentialCell(features)
        self.pool = GlobalAvgPooling()
        self.dropout = nn.Dropout(keep_prob=1 - drop_rate)
        self.classifier = nn.Dense(last_channel, num_classes)
        self._initialize_weights()

    def _initialize_weights(self) -> None:
        for _, cell in self.cells_and_names():
            if isinstance(cell, nn.Conv2d):
                cell.weight.set_data(
                    init.initializer(init.HeNormal(mode='fan_out', nonlinearity='relu'),
                                     cell.weight.shape, cell.weight.dtype))
                if cell.bias is not None:
                    cell.bias.set_data(
                        init.initializer('zeros', cell.bias.shape, cell.bias.dtype))
            elif isinstance(cell, nn.BatchNorm2d):
                cell.gamma.set_data(init.initializer('ones', cell.gamma.shape, cell.gamma.dtype))
                cell.beta.set_data(init.initializer('zeros', cell.beta.shape, cell.beta.dtype))
            elif isinstance(cell, nn.Dense):
                cell.weight.set_data(
                    init.initializer(init.HeUniform(mode='fan_out', nonlinearity='sigmoid'),
                                     cell.weight.shape, cell.weight.dtype))
                if cell.bias is not None:
                    cell.bias.set_data(init.initializer('zeros', cell.bias.shape, cell.bias.dtype))

    def forward_features(self, x: Tensor) -> Tensor:
        x = self.features(x)
        return x

    def forward_head(self, x: Tensor) -> Tensor:
        x = self.pool(x)
        x = self.dropout(x)
        x = self.classifier(x)
        return x

    def construct(self, x: Tensor) -> Tensor:
        x = self.forward_features(x)
        x = self.forward_head(x)
        return x


@register_model
def mnasnet0_5(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> Mnasnet:
    default_cfg = default_cfgs['mnasnet0.5']
    model = Mnasnet(alpha=0.5, in_channels=in_channels, num_classes=num_classes, **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def mnasnet0_75(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> Mnasnet:
    default_cfg = default_cfgs['mnasnet0.75']
    model = Mnasnet(alpha=0.75, in_channels=in_channels, num_classes=num_classes, **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def mnasnet1_0(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> Mnasnet:
    default_cfg = default_cfgs['mnasnet1.0']
    model = Mnasnet(alpha=1.0, in_channels=in_channels, num_classes=num_classes, **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def mnasnet1_4(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> Mnasnet:
    default_cfg = default_cfgs['mnasnet1.4']
    model = Mnasnet(alpha=1.4, in_channels=in_channels, num_classes=num_classes, **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model
