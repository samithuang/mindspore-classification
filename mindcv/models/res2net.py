# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""
MindSpore implementation of `Res2Net`.
Refer to: A New Multi-scale Backbone Architecture
"""

import math
import mindspore.nn as nn
from mindspore.ops import operations as P
import mindspore.common.initializer as init
from mission.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD, DEFAULT_CROP_PCT
from .layers.conv_norm_act import Conv2dNormActivation
from .utils import load_pretrained
from .registry import register_model

'''
1. 将model zoo里的 _conv3x3、_conv1x1、_conv7x7、_bn、_bn_last这些封装操作写法冗余，很乱，
直接用公用的Conv2dNormActivation就可以替换，能实现所有的功能

2. 关于初始化权重可以写个统一的_init_weights函数，放在里面统一执行。
3. 创建不同规格的模型使用@register_model进行创建，和timm保持一致
'''

__all__ = ['Res2Net', 'Residual2Block']


def _cfg(url='', **kwargs):
    return {
        'url': url,
        'num_classes': 1000,
        'dataset_transform': {
            'transforms_imagenet_train': {
                'image_resize': 224,
                'scale': (0.08, 1.0),
                'ratio': (0.75, 1.333),
                'hflip': 0.5,
                'interpolation': 'bilinear',
                'mean': IMAGENET_DEFAULT_MEAN,
                'std': IMAGENET_DEFAULT_STD,
            },
            'transforms_imagenet_eval': {
                'image_resize': 224,
                'crop_pct': DEFAULT_CROP_PCT,
                'interpolation': 'bilinear',
                'mean': IMAGENET_DEFAULT_MEAN,
                'std': IMAGENET_DEFAULT_STD,
            },
        },
        'first_conv': 'conv1', 'classifier': 'fc',
        **kwargs
    }


default_cfgs = {
    'res2net50': _cfg(url=''),
    'res2net101': _cfg(url=''),
    'res2net152': _cfg(url=''),
}


class Residual2Block(nn.Cell):
    expansion = 4

    # 先去掉了use_se和se_block参数，后面再实现SE部分，这里先不实现了，实现的有些乱，影响代码的可读性
    # 去掉了res_base参数，没有必要。
    # 保留down_sample, 和timm保持一致

    def __init__(
            self,
            in_channel,
            out_channel,
            stride=1,
            baseWidth=26,
            scale=4,
            stype="normal",
            name="Res2Net"
    ):
        super(Residual2Block, self).__init__()
        self.name = name
        self.stride = stride
        assert scale > 1, "Res2Net is ResNet when scale = 1"
        width = int(math.floor(out_channel //
                               self.expansion * (baseWidth / 64.0)))
        channel = width * scale
        self.conv1 = Conv2dNormActivation(
            in_planes=in_channel,
            out_planes=channel,
            kernel_size=1,
            stride=1,
            pad_mode="same",
            norm=nn.BatchNorm2d,
            activation=nn.ReLU
        )
        if stype == "stage":
            self.pool = nn.AvgPool2d(
                kernel_size=3, stride=stride, pad_mode="same")
        self.convs = nn.CellList()
        self.bns = nn.CellList()

        for _ in range(scale - 1):
            self.convs.append(
                Conv2dNormActivation(
                    in_planes=width,
                    out_planes=width,
                    kernel_size=3,
                    stride=stride,
                    pad_mode="same",
                    norm=nn.BatchNorm2d,
                    activation=nn.ReLU
                )
            )
        self.conv3 = Conv2dNormActivation(
            in_planes=channel,
            out_planes=out_channel,
            kernel_size=1,
            stride=1,
            pad_mode="same",
            norm=nn.BatchNorm2d,
            activation=None
        )
        self.relu = nn.ReLU()
        self.down_sample = False

        if stride != 1 or in_channel != out_channel:
            self.down_sample = True
        self.down_sample_layer = None

        if self.down_sample:
            if stride == 1:
                self.down_sample_layer = nn.SequentialCell(
                    [
                        Conv2dNormActivation(
                            in_planes=in_channel,
                            out_planes=out_channel,
                            kernel_size=1,
                            stride=stride,
                            pad_mode="same",
                            norm=nn.BatchNorm2d,
                            activation=None
                        )
                    ]
                )
            else:
                self.down_sample_layer = nn.SequentialCell(
                    [
                        nn.MaxPool2d(kernel_size=2, stride=2, pad_mode="same"),
                        Conv2dNormActivation(
                            in_planes=in_channel,
                            out_planes=out_channel,
                            kernel_size=1,
                            stride=1,
                            pad_mode="same",
                            norm=nn.BatchNorm2d,
                            activation=None
                        )

                    ]
                )
        self.add = P.Add()
        self.inplace_add = P.InplaceAdd((0, 1, 2, 3))
        self.scale = scale
        self.width = width
        self.stride = stride
        self.stype = stype
        self.split = P.Split(axis=1, output_num=scale)
        self.cat = P.Concat(axis=1)
        self._initialize_weights()

    def construct(self, x):
        identity = x
        out = self.conv1(x)
        spx = self.split(out)
        sp = self.convs[0](spx[0])
        out = sp

        for i in range(1, self.scale - 1):
            if self.stype == "stage":
                sp = spx[i]
            else:
                sp = sp[:, :, :, :]  # to avoid bug in mindspore
                sp = sp + spx[i]
            sp = self.convs[i](sp)
            out = self.cat((out, sp))

        if self.stype == "normal":
            out = self.cat((out, spx[self.scale - 1]))
        elif self.stype == "stage":
            out = self.cat((out, self.pool(spx[self.scale - 1])))
        out = self.conv3(out)

        if self.down_sample:
            identity = self.down_sample_layer(identity)
        out = out + identity
        out = self.relu(out)

        return out

    # 使用统一的初始化函数进行初始化，modelzoo中对不同层的参数使用了不同的初始化方式和数值
    # 在这里先对相同类型的层使用相同的初始化方式
    def _initialize_weights(self):
        for name, m in self.cells_and_names():
            if isinstance(m, nn.Conv2d):
                m.weight.set_data(init.initializer(init.HeNormal(mode="fan_in", nonlinearity="leaky_relu"), m.weight.shape))  # 改成对应的kaiming初始化方式
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))
            elif isinstance(m, nn.BatchNorm2d) or isinstance(m, nn.BatchNorm1d):
                if name == self.conv3 and self.name == "Res2Net152":
                    m.gamma.set_data(init.initializer(init.Constant(1), m.gamma.shape))
                else:
                    m.gamma.set_data(init.initializer(init.Constant(1), m.gamma.shape))
                if m.beta is not None:
                    m.beta.set_data(init.initializer(init.Constant(0.0001), m.beta.shape))
            elif isinstance(m, nn.Dense):
                m.weight.set_data(init.initializer(init.Normal(0.01, 0), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))


class Res2Net(nn.Cell):

    # 删掉use_se和se_block部分，先不实现se模块
    # 使用公用的Conv2dNormActivation替换连续的conv, bn, activation操作

    def __init__(
            self,
            block,
            layer_nums,
            in_channels,
            out_channels,
            strides,
            num_classes,
            name="Res2Net"
    ):
        super(Res2Net, self).__init__()
        if not len(layer_nums) == len(in_channels) == len(out_channels) == 4:
            raise ValueError(
                "the length of layer_num, in_channels, out_channels list must be 4!"
            )
        self.name = name
        self.conv1_0 = Conv2dNormActivation(
            in_planes=3,
            out_planes=32,
            kernel_size=3,
            stride=2,
            pad_mode="same",
            norm=nn.BatchNorm2d,
            activation=nn.ReLU
        )
        self.conv1_1 = Conv2dNormActivation(
            in_planes=32,
            out_planes=32,
            kernel_size=3,
            stride=1,
            pad_mode="same",
            norm=nn.BatchNorm2d,
            activation=nn.ReLU
        )
        self.conv1_2 = Conv2dNormActivation(
            in_planes=32,
            out_planes=64,
            kernel_size=3,
            stride=1,
            pad_mode="same",
            norm=nn.BatchNorm2d,   # 需要添加1个self.res_base的判断
            activation=nn.ReLU
        )
        self.maxpool = nn.MaxPool2d(
            kernel_size=3, stride=2, pad_mode="same")

        self.layer1 = self._make_layer(
            block,
            layer_nums[0],
            in_channel=in_channels[0],
            out_channel=out_channels[0],
            stride=strides[0],
        )
        self.layer2 = self._make_layer(
            block,
            layer_nums[1],
            in_channel=in_channels[1],
            out_channel=out_channels[1],
            stride=strides[1],
        )
        self.layer3 = self._make_layer(
            block,
            layer_nums[2],
            in_channel=in_channels[2],
            out_channel=out_channels[2],
            stride=strides[2],
        )
        self.layer4 = self._make_layer(
            block,
            layer_nums[3],
            in_channel=in_channels[3],
            out_channel=out_channels[3],
            stride=strides[3],
        )
        self.mean = P.ReduceMean(keep_dims=True)
        self.flatten = nn.Flatten()

        self.end_point = nn.Dense(out_channels[3], num_classes, has_bias=True)
        self._initialize_weights()

    def _make_layer(
            self,
            block,
            layer_num,
            in_channel,
            out_channel,
            stride
    ):
        layers = []

        res2net_block = block(
            in_channel, out_channel, stride=stride, stype="stage", name=self.name
        )
        layers.append(res2net_block)
        for _ in range(1, layer_num):
            res2net_block = block(
                out_channel, out_channel, stride=1)
            layers.append(res2net_block)
        return nn.SequentialCell(layers)

    def construct(self, x):

        x = self.conv1_0(x)
        x = self.conv1_1(x)
        x = self.conv1_2(x)

        c1 = self.maxpool(x)

        c2 = self.layer1(c1)
        c3 = self.layer2(c2)
        c4 = self.layer3(c3)
        c5 = self.layer4(c4)

        out = self.mean(c5, (2, 3))
        out = self.flatten(out)
        out = self.end_point(out)

        return out

    # 使用统一的初始化函数进行初始化，modelzoo中对不同层的参数使用了不同的初始化方式和数值
    # 在这里先对相同类型的层使用相同的初始化方式

    def _initialize_weights(self):
        for name, m in self.cells_and_names():
            if isinstance(m, nn.Conv2d):
                m.weight.set_data(init.initializer(init.Normal(0.01, 0), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))
            elif isinstance(m, nn.BatchNorm2d) or isinstance(m, nn.BatchNorm1d):
                m.gamma.set_data(init.initializer(init.Constant(1), m.gamma.shape))
                if m.beta is not None:
                    m.beta.set_data(init.initializer(init.Constant(0.0001), m.beta.shape))
            elif isinstance(m, nn.Dense):
                m.weight.set_data(init.initializer(init.Normal(0.01, 0), m.weight.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))


# 创建不同规格的模型使用@register_model进行创建，和timm保持一致
@register_model
def res2net50(pretrained: bool = False, class_num=10, in_channels=3):
    default_cfg = default_cfgs['res2net50']
    model = Res2Net(
        Residual2Block,
        [3, 4, 6, 3],
        [64, 256, 512, 1024],
        [256, 512, 1024, 2048],
        [1, 2, 2, 2],
        class_num,
    )
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=class_num, in_channels=in_channels)

    return model


@register_model
def res2net101(pretrained: bool = False, class_num=1001, in_channels=3):
    default_cfg = default_cfgs['res2net101']
    model = Res2Net(
        Residual2Block,
        [3, 4, 23, 3],
        [64, 256, 512, 1024],
        [256, 512, 1024, 2048],
        [1, 2, 2, 2],
        class_num,
    )
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=class_num, in_channels=in_channels)

    return model


@register_model
def res2net152(pretrained: bool = False, class_num=1001, in_channels=3):
    default_cfg = default_cfgs['res2net152']
    model = Res2Net(
        Residual2Block,
        [3, 8, 36, 3],
        [64, 256, 512, 1024],
        [256, 512, 1024, 2048],
        [1, 2, 2, 2],
        class_num,
        "Res2Net152"
    )
    model.dataset_transform = default_cfg['dataset_transform']

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=class_num, in_channels=in_channels)

    return model
