# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""
MindSpore implementation of `BEiT`.
Refer to BEiT: BERT Pre-Training of Image Transformers
"""

import math
import numpy as np
from functools import partial
from typing import Optional, Tuple

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops
import mindspore.common.initializer as init

__all__ = [
    "BEiT",
    "beit_base_patch16_224",
    "beit_base_patch16_224_in22k",
    "beit_base_patch16_384",
    "beit_large_patch16_224",
    "beit_large_patch16_224_in22k",
    "beit_large_patch16_384",
    "beit_large_patch16_512"
]


# fixme: we use numpy instead of mindspore, for operator lacking.
#  1. meshgrid does not support cpu. 2. Initialize primitive operator, then use as function is weird!
def gen_relative_position_index(window_size: Tuple[int, int]) -> np.ndarray:
    num_relative_distance = (2 * window_size[0] - 1) * (2 * window_size[1] - 1) + 3
    # cls to token & token 2 cls & cls to cls
    # get pair-wise relative position index for each token inside the window
    window_area = window_size[0] * window_size[1]
    coords = np.stack(np.meshgrid(
        np.arange(window_size[0]),  # width
        np.arange(window_size[1])  # height
    ))  # 2, Wh, Ww
    coords_flatten = np.reshape(coords, (2, -1))  # 2, Wh*Ww
    relative_coords = coords_flatten[:, :, None] - coords_flatten[:, None, :]  # 2, Wh*Ww, Wh*Ww
    relative_coords = np.transpose(relative_coords, (1, 2, 0))  # Wh*Ww, Wh*Ww, 2
    relative_coords[:, :, 0] += window_size[0] - 1  # shift to start from 0
    relative_coords[:, :, 1] += window_size[1] - 1
    relative_coords[:, :, 0] *= 2 * window_size[1] - 1
    relative_position_index = np.zeros((window_area + 1,) * 2, dtype=relative_coords.dtype)
    relative_position_index[1:, 1:] = relative_coords.sum(-1)  # Wh*Ww, Wh*Ww
    relative_position_index[0, 0:] = num_relative_distance - 3
    relative_position_index[0:, 0] = num_relative_distance - 2
    relative_position_index[0, 0] = num_relative_distance - 1
    return relative_position_index


# fixme: DropPath is copied from layers/drop.py
class DropPath(nn.Cell):
    """Drop paths (Stochastic Depth) per sample (when applied in main path of residual blocks)."""

    def __init__(self, drop_prob: float = 0., scale_by_keep: bool = True):
        super(DropPath, self).__init__()
        self.drop_prob = drop_prob
        self.scale_by_keep = scale_by_keep
        self.cast = ops.Cast()

    def construct(self, x):
        if self.drop_prob == 0. or not self.training:
            return x
        keep_prob = 1 - self.drop_prob
        shape = (x.shape[0],) + (1,) * (x.ndim - 1)  # work with diff dim tensors, not just 2D ConvNets
        # fixme: tensor.bernoulli_ is not supported.
        #  mindspore.nn.probability.distribution.Bernoulli does not support CPU.
        random_tensor = init.initializer(init.Uniform(0.5), shape, x.dtype) + 0.5
        random_tensor = self.cast(random_tensor < self.drop_prob, random_tensor.dtype)
        if keep_prob > 0.0 and self.scale_by_keep:
            random_tensor /= keep_prob
        return x * random_tensor

    def extra_repr(self):
        return f'drop_prob={round(self.drop_prob, 3):0.3f}'


# fixme: mindspore does not support Identity
class Identity(nn.Cell):
    """Identity layer. For the convenience of model structure definition."""

    def construct(self, x):
        return x


# fixme: MLP is copied from layers/mlp.py
class Mlp(nn.Cell):
    """ MLP as used in Vision Transformer and related networks."""

    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, bias=True, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        bias = bias if isinstance(bias, tuple) else (bias,) * 2
        drop_probs = drop if isinstance(drop, tuple) else (drop,) * 2

        self.fc1 = nn.Dense(in_features, hidden_features, has_bias=bias[0])
        self.act = act_layer()
        self.drop1 = nn.Dropout(keep_prob=1 - drop_probs[0])
        self.fc2 = nn.Dense(hidden_features, out_features, has_bias=bias[1])
        self.drop2 = nn.Dropout(keep_prob=1 - drop_probs[1])

    def construct(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop1(x)
        x = self.fc2(x)
        x = self.drop2(x)
        return x


class Attention(nn.Cell):
    """Attention layer in Transformer."""

    def __init__(self, dim, num_heads=8, qkv_bias=False, attn_drop=0.,
                 proj_drop=0., window_size=None, attn_head_dim=None):
        super().__init__()
        self.num_heads = num_heads
        head_dim = dim // num_heads
        if attn_head_dim is not None:
            head_dim = attn_head_dim
        all_head_dim = head_dim * self.num_heads
        self.scale = head_dim ** -0.5

        self.qkv = nn.Dense(dim, all_head_dim * 3, has_bias=False)
        if qkv_bias:
            # fixme: what's the different between
            #  1. mindspore.Tensor(np.zeros(shape))
            #  2. mindspore.numpy.zeros(shape)
            #  3. zeros = mindspore.ops.Zeros(); zeros(shape)
            #  which one is recommended?
            self.q_bias = ms.Parameter(ms.Tensor(np.zeros(all_head_dim), dtype=ms.float32))
            # fixme: mindspore still does NOT support buffer in nn.Cell. Why? 
            #  It must be noted that setting requires_grad=False is NOT equivalent to using buffer as in pytorch.
            #  self.register_buffer('k_bias', torch.zeros(all_head_dim), persistent=False)
            self.k_bias = ms.Parameter(ms.Tensor(np.zeros(all_head_dim), dtype=ms.float32), requires_grad=False)
            self.v_bias = ms.Parameter(ms.Tensor(np.zeros(all_head_dim), dtype=ms.float32))
        else:
            self.q_bias = None
            self.k_bias = None
            self.v_bias = None

        if window_size:
            self.window_size = window_size
            self.num_relative_distance = (2 * window_size[0] - 1) * (2 * window_size[1] - 1) + 3
            self.relative_position_bias_table = ms.Parameter(
                ms.numpy.zeros((self.num_relative_distance, num_heads), dtype=ms.float32))  # 2*Wh-1 * 2*Ww-1, nH
            self.relative_position_index = ms.Parameter(
                ms.Tensor(gen_relative_position_index(window_size)), requires_grad=False)
        else:
            self.window_size = None
            self.relative_position_bias_table = None
            self.relative_position_index = None

        self.attn_drop = nn.Dropout(keep_prob=1 - attn_drop)
        self.proj = nn.Dense(all_head_dim, dim)
        self.proj_drop = nn.Dropout(keep_prob=1 - proj_drop)

        self.concat = ops.Concat()
        self.reshape = ops.Reshape()
        self.bias_add = ops.BiasAdd()
        self.matmul = ops.MatMul(transpose_b=True)
        self.bmm = ops.BatchMatMul()
        self.softmax = ops.Softmax()

    def _get_rel_pos_bias(self):
        relative_position_bias = self.relative_position_bias_table[self.relative_position_index.view(-1)].view(
            self.window_size[0] * self.window_size[1] + 1,
            self.window_size[0] * self.window_size[1] + 1,
            -1)  # Wh*Ww,Wh*Ww,nH
        relative_position_bias = relative_position_bias.transpose(2, 0, 1)  # nH, Wh*Ww, Wh*Ww
        return relative_position_bias.expand_dims(0)

    # fixme: mindspore does not support F.linear
    def linear(self, x, weight, bias):
        x_shape = x.shape
        if len(x_shape) != 2:
            x = self.reshape(x, (-1, x_shape[-1]))
        x = self.matmul(x, weight)
        if bias is not None:
            x = self.bias_add(x, bias)
        if len(x_shape) != 2:
            out_shape = x_shape[:-1] + (-1,)
            x = self.reshape(x, out_shape)
        return x

    def construct(self, x, shared_rel_pos_bias: Optional[ms.Tensor] = None):
        b, n, c = x.shape

        qkv_bias = self.concat((self.q_bias, self.k_bias, self.v_bias)) if self.q_bias is not None else None
        qkv = self.linear(x, self.qkv.weight, qkv_bias)
        qkv = qkv.reshape(b, n, 3, self.num_heads, -1).transpose(2, 0, 3, 1, 4)
        # fixme: mindspore does not support unbind. Can we use tensor as tuple?
        q, k, v = qkv  # (3, b, num_heads, num_tokens, head_dim)

        q = q * self.scale
        # fixme: swapaxes, transpose, permute?
        # fixme: @ is not supported.
        attn = self.bmm(q, k.swapaxes(-2, -1))

        if self.relative_position_bias_table is not None:
            attn = attn + self._get_rel_pos_bias()
        if shared_rel_pos_bias is not None:
            attn = attn + shared_rel_pos_bias

        # fixme: tensor.softmax is not supported!
        attn = self.softmax(attn)
        attn = self.attn_drop(attn)

        x = self.bmm(attn, v).swapaxes(1, 2).reshape(b, n, -1)
        x = self.proj(x)
        x = self.proj_drop(x)
        return x


class Block(nn.Cell):
    """Basic Block of Transformer."""

    def __init__(
            self, dim, num_heads, mlp_ratio=4., qkv_bias=False, drop=0., attn_drop=0.,
            drop_path=0., init_values=None, act_layer=nn.GELU, norm_layer=nn.LayerNorm,
            window_size=None, attn_head_dim=None):
        super().__init__()
        self.norm1 = norm_layer((dim,))
        self.attn = Attention(
            dim, num_heads=num_heads, qkv_bias=qkv_bias, attn_drop=attn_drop, proj_drop=drop,
            window_size=window_size, attn_head_dim=attn_head_dim)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else Identity()
        self.norm2 = norm_layer((dim,))
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

        if init_values:
            self.gamma_1 = ms.Parameter(init_values * ms.numpy.ones(dim), requires_grad=True)
            self.gamma_2 = ms.Parameter(init_values * ms.numpy.ones(dim), requires_grad=True)
        else:
            self.gamma_1, self.gamma_2 = None, None

    def construct(self, x, shared_rel_pos_bias: Optional[ms.Tensor] = None):
        if self.gamma_1 is None:
            x = x + self.drop_path(self.attn(self.norm1(x), shared_rel_pos_bias=shared_rel_pos_bias))
            x = x + self.drop_path(self.mlp(self.norm2(x)))
        else:
            x = x + self.drop_path(self.gamma_1 * self.attn(self.norm1(x), shared_rel_pos_bias=shared_rel_pos_bias))
            x = x + self.drop_path(self.gamma_2 * self.mlp(self.norm2(x)))
        return x


class RelativePositionBias(nn.Cell):
    """Layer with trainable relative position bias as param."""

    def __init__(self, window_size, num_heads):
        super().__init__()
        self.window_size = window_size
        self.window_area = window_size[0] * window_size[1]
        num_relative_distance = (2 * window_size[0] - 1) * (2 * window_size[1] - 1) + 3
        self.relative_position_bias_table = ms.Parameter(ms.numpy.zeros((num_relative_distance, num_heads)))
        # trunc_normal_(self.relative_position_bias_table, std=.02)
        self.relative_position_index = ms.Parameter(
            ms.Tensor(gen_relative_position_index(window_size)), requires_grad=False)

    def construct(self):
        relative_position_bias = self.relative_position_bias_table[self.relative_position_index.view(-1)].view(
            self.window_area + 1, self.window_area + 1, -1)  # Wh*Ww,Wh*Ww,nH
        return relative_position_bias.transpose(2, 0, 1)  # nH, Wh*Ww, Wh*Ww


# fixme: copy from layers/patch_embed.py
class PatchEmbed(nn.Cell):
    """2D Image to Patch Embedding."""

    def __init__(self, img_size=224, patch_size=16, in_chans=3, embed_dim=768, norm_layer=None, flatten=True):
        super().__init__()
        img_size = img_size if isinstance(img_size, tuple) else (img_size,) * 2
        patch_size = patch_size if isinstance(patch_size, tuple) else (patch_size,) * 2
        self.img_size = img_size
        self.patch_size = patch_size
        self.grid_size = (img_size[0] // patch_size[0], img_size[1] // patch_size[1])
        self.num_patches = self.grid_size[0] * self.grid_size[1]
        self.flatten = flatten

        self.proj = nn.Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=patch_size)
        self.norm = norm_layer((embed_dim,)) if norm_layer else Identity()

    def construct(self, x):
        # b, c, h, w = x.shape
        # _assert(h == self.img_size[0], f"Input image height ({h}) doesn't match model ({self.img_size[0]}).")
        # _assert(w == self.img_size[1], f"Input image width ({w}) doesn't match model ({self.img_size[1]}).")
        x = self.proj(x)
        b, c, h, w = x.shape
        if self.flatten:
            x = x.view(b, c, h * w).swapaxes(1, 2)  # (b,c,h,w) -> (b,n,c)
        x = self.norm(x)
        return x


class BEiT(nn.Cell):
    r"""BEiT model class, based on
    `"BEiT: BERT Pre-Training of Image Transformers" <https://arxiv.org/abs/2106.08254>`_
    """

    def __init__(
            self, img_size=224, patch_size=16, in_chans=3, num_classes=1000, global_pool='avg',
            embed_dim=768, depth=12, num_heads=12, mlp_ratio=4., qkv_bias=True, drop_rate=0.,
            attn_drop_rate=0., drop_path_rate=0., norm_layer=partial(nn.LayerNorm, epsilon=1e-6),
            init_values=None, use_abs_pos_emb=True, use_rel_pos_bias=False, use_shared_rel_pos_bias=False,
            head_init_scale=0.001):
        super().__init__()
        self.num_classes = num_classes
        self.global_pool = global_pool
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        self.grad_checkpointing = False

        self.patch_embed = PatchEmbed(
            img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=embed_dim)
        num_patches = self.patch_embed.num_patches

        self.cls_token = ms.Parameter(ms.numpy.zeros((1, 1, embed_dim)))
        # self.mask_token = ms.Parameter(ms.numpy.zeros((1, 1, embed_dim)))
        self.pos_embed = ms.Parameter(ms.numpy.zeros((1, num_patches + 1, embed_dim))) if use_abs_pos_emb else None
        self.pos_drop = nn.Dropout(keep_prob=1 - drop_rate)

        if use_shared_rel_pos_bias:
            self.rel_pos_bias = RelativePositionBias(window_size=self.patch_embed.grid_size, num_heads=num_heads)
        else:
            self.rel_pos_bias = None

        dpr = [x for x in np.linspace(0, drop_path_rate, depth)]  # stochastic depth decay rule
        self.blocks = nn.CellList([
            Block(
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=dpr[i], norm_layer=norm_layer,
                init_values=init_values, window_size=self.patch_embed.grid_size if use_rel_pos_bias else None)
            for i in range(depth)])
        use_fc_norm = self.global_pool == 'avg'
        self.norm = Identity() if use_fc_norm else norm_layer((embed_dim,))
        self.fc_norm = norm_layer((embed_dim,)) if use_fc_norm else None
        self.head = nn.Dense(embed_dim, num_classes) if num_classes > 0 else Identity()
        self.concat = ops.Concat(axis=1)

        self._init_weights()
        if self.pos_embed is not None:
            self.pos_embed.set_data(init.initializer(init.TruncatedNormal(sigma=.02), self.pos_embed.data.shape))
        self.cls_token.set_data(init.initializer(init.TruncatedNormal(sigma=.02), self.cls_token.data.shape))
        # trunc_normal_(self.mask_token, std=.02)
        self.fix_init_weight()
        if isinstance(self.head, nn.Dense):
            self.head.weight.set_data(init.initializer(init.TruncatedNormal(sigma=.02), self.head.weight.data.shape))
            self.head.weight.set_data(self.head.weight.data * head_init_scale)
            self.head.bias.set_data(self.head.bias.data * head_init_scale)

    def fix_init_weight(self):
        def rescale(param, layer_id):
            param /= math.sqrt(2.0 * layer_id)

        for layer_id, layer in enumerate(self.blocks):
            rescale(layer.attn.proj.weight.data, layer_id + 1)
            rescale(layer.mlp.fc2.weight.data, layer_id + 1)

    def _init_weights(self):
        for name, m in self.cells_and_names():
            if isinstance(m, nn.Dense):
                m.weight.set_data(init.initializer(init.TruncatedNormal(sigma=.02), m.weight.data.shape))
                if m.bias is not None:
                    m.bias.set_data(init.initializer(init.Constant(0), m.bias.shape))
            elif isinstance(m, nn.LayerNorm):
                m.gamma.set_data(init.initializer(init.Constant(1), m.gamma.shape))
                m.beta.set_data(init.initializer(init.Constant(0), m.beta.shape))

    def forward_features(self, x):
        x = self.patch_embed(x)
        # fixme: expand is not supported.
        x = self.concat((self.cls_token.repeat(x.shape[0], axis=0), x))
        if self.pos_embed is not None:
            x = x + self.pos_embed
        x = self.pos_drop(x)

        rel_pos_bias = self.rel_pos_bias() if self.rel_pos_bias is not None else None
        for blk in self.blocks:
            # fixme: grad_checking?
            x = blk(x, shared_rel_pos_bias=rel_pos_bias)
        x = self.norm(x)
        return x

    def forward_head(self, x, pre_logits: bool = False):
        if self.fc_norm is not None:
            x = x[:, 1:].mean(axis=1)
            x = self.fc_norm(x)
        else:
            x = x[:, 0]
        return x if pre_logits else self.head(x)

    def construct(self, x):
        x = self.forward_features(x)
        x = self.forward_head(x)
        return x


def _create_beit(variant, pretrained=False, **kwargs):
    # FIXME an updated filter fn needed to interpolate rel pos emb if fine tuning to diff model sizes
    model = BEiT(**kwargs)
    return model


def beit_base_patch16_224(pretrained=False, **kwargs):
    model_kwargs = dict(
        patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=0.1, **kwargs)
    model = _create_beit('beit_base_patch16_224', pretrained=pretrained, **model_kwargs)
    return model


def beit_base_patch16_384(pretrained=False, **kwargs):
    model_kwargs = dict(
        img_size=384, patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=0.1, **kwargs)
    model = _create_beit('beit_base_patch16_384', pretrained=pretrained, **model_kwargs)
    return model


def beit_base_patch16_224_in22k(pretrained=False, **kwargs):
    model_kwargs = dict(
        patch_size=16, embed_dim=768, depth=12, num_heads=12, mlp_ratio=4,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=0.1, **kwargs)
    model = _create_beit('beit_base_patch16_224_in22k', pretrained=pretrained, **model_kwargs)
    return model


def beit_large_patch16_224(pretrained=False, **kwargs):
    model_kwargs = dict(
        patch_size=16, embed_dim=1024, depth=24, num_heads=16, mlp_ratio=4, qkv_bias=True,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=1e-5, **kwargs)
    model = _create_beit('beit_large_patch16_224', pretrained=pretrained, **model_kwargs)
    return model


def beit_large_patch16_384(pretrained=False, **kwargs):
    model_kwargs = dict(
        img_size=384, patch_size=16, embed_dim=1024, depth=24, num_heads=16, mlp_ratio=4, qkv_bias=True,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=1e-5, **kwargs)
    model = _create_beit('beit_large_patch16_384', pretrained=pretrained, **model_kwargs)
    return model


def beit_large_patch16_512(pretrained=False, **kwargs):
    model_kwargs = dict(
        img_size=512, patch_size=16, embed_dim=1024, depth=24, num_heads=16, mlp_ratio=4, qkv_bias=True,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=1e-5, **kwargs)
    model = _create_beit('beit_large_patch16_512', pretrained=pretrained, **model_kwargs)
    return model


def beit_large_patch16_224_in22k(pretrained=False, **kwargs):
    model_kwargs = dict(
        patch_size=16, embed_dim=1024, depth=24, num_heads=16, mlp_ratio=4, qkv_bias=True,
        use_abs_pos_emb=False, use_rel_pos_bias=True, init_values=1e-5, **kwargs)
    model = _create_beit('beit_large_patch16_224_in22k', pretrained=pretrained, **model_kwargs)
    return model


if __name__ == "__main__":
    import numpy as np
    import mindspore
    from mindspore import Tensor
    from mindspore import context
    context.set_context(mode=mindspore.PYNATIVE_MODE)

    net = beit_base_patch16_224()
    dummy_input = Tensor(np.random.rand(8, 3, 224, 224), dtype=mindspore.float32)
    y = net(dummy_input)
    print("Shape of out :", y.shape)
