# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================

"""
MindSpore implementation of `TNT`.
Refer to Transformer in Transformer(TNT).
"""

import math
import numpy as np
from scipy.stats import truncnorm

import mindspore as ms
import mindspore.nn as nn
import mindspore.ops as ops
import mindspore.common.initializer as init
from mindspore import Tensor, Parameter

__all__ = [
    "TNT",
    "tnt_s_patch16_224",
    "tnt_b_patch16_224"
]


def make_divisible(v, divisor=8, min_value=None):
    min_value = min_value or divisor
    new_v = max(min_value, int(v + divisor / 2) // divisor * divisor)
    # Make sure that round down does not go down by more than 10%.
    if new_v < 0.9 * v:
        new_v += divisor
    return new_v


def trunc_array(shape, sigma=0.02):
    return truncnorm.rvs(-2, 2, loc=0, scale=sigma, size=shape, random_state=None)


class Identity(nn.Cell):
    """Identity layer."""

    def construct(self, x):
        return x


class DropPath(nn.Cell):
    """Drop paths (Stochastic Depth) per sample (when applied in main path of residual blocks)."""

    def __init__(self, drop_prob):
        super(DropPath, self).__init__()
        self.drop = nn.Dropout(keep_prob=1 - drop_prob)

    def construct(self, x):
        if not self.training:
            return x
        shape = (x.shape[0],) + (1,) * (x.ndim - 1)
        mask = Tensor(np.ones(shape), dtype=x.dtype)
        out = self.drop(mask)
        out = out * x
        return out


class UnfoldKernelEqPatch(nn.Cell):
    """UnfoldKernelEqPatch with better performance"""

    def __init__(self, kernel_size, strides):
        super(UnfoldKernelEqPatch, self).__init__()
        assert kernel_size == strides
        self.kernel_size = kernel_size

    def construct(self, x):
        b, c, h, w = x.shape
        x = ops.reshape(x, (b, c, h // self.kernel_size[0], self.kernel_size[0], w))
        x = ops.transpose(x, (0, 2, 1, 3, 4))
        x = ops.reshape(x, (-1, c, self.kernel_size[0], w // self.kernel_size[1], self.kernel_size[1]))
        x = ops.transpose(x, (0, 3, 1, 2, 4))
        x = ops.reshape(x, (-1, c, self.kernel_size[0], self.kernel_size[1]))
        return x


class Mlp(nn.Cell):
    """MLP layer"""

    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Dense(in_channels=in_features, out_channels=hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Dense(in_channels=hidden_features, out_channels=out_features, has_bias=False)
        self.drop = nn.Dropout(keep_prob=1.0 - drop) if drop > 0. else Identity()

    def construct(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x


class SE(nn.Cell):
    """Squeeze-and-Excitation Block"""

    def __init__(self, dim, hidden_ratio=None):
        super().__init__()
        hidden_ratio = hidden_ratio or 1
        self.dim = dim
        hidden_dim = int(dim * hidden_ratio)
        self.fc = nn.SequentialCell([
            nn.LayerNorm((dim,), epsilon=1e-05),
            nn.Dense(in_channels=dim, out_channels=hidden_dim, has_bias=False),
            nn.ReLU(),
            nn.Dense(in_channels=hidden_dim, out_channels=dim, has_bias=False),
            nn.Tanh()
        ])
        self.mean = ops.ReduceMean(keep_dims=True)

    def construct(self, x):
        a = self.mean(x, 1)  # b, 1, c
        a = self.fc(a)
        x = a * x
        return x


class Attention(nn.Cell):
    """Attention layer in Transformer."""

    def __init__(self, dim, hidden_dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
        super().__init__()
        self.hidden_dim = hidden_dim
        self.num_heads = num_heads
        head_dim = hidden_dim // num_heads
        self.head_dim = head_dim
        # NOTE scale factor was wrong in my original version, can set manually to be compat with prev weights
        self.scale = qk_scale or head_dim ** -0.5

        # self.qk = nn.Dense(in_channels=dim, out_channels=hidden_dim * 2, has_bias=qkv_bias)
        self.q = nn.Dense(in_channels=dim, out_channels=hidden_dim, has_bias=qkv_bias)
        self.k = nn.Dense(in_channels=dim, out_channels=hidden_dim, has_bias=qkv_bias)
        self.v = nn.Dense(in_channels=dim, out_channels=dim, has_bias=qkv_bias)
        self.attn_drop = nn.Dropout(keep_prob=1.0 - attn_drop)
        self.proj = nn.Dense(in_channels=dim, out_channels=dim, has_bias=False)
        self.proj_drop = nn.Dropout(keep_prob=1.0 - proj_drop)
        self.softmax = nn.Softmax(axis=-1)
        self.bmm = ops.BatchMatMul()

    def construct(self, x):
        b, n, _ = x.shape
        # qk = ops.Reshape()(self.qk(x), (b, n, 2, self.num_heads, self.head_dim))
        # qk = ops.Transpose()(qk, (2, 0, 3, 1, 4))
        q = ops.reshape(self.q(x), (b, n, self.num_heads, self.head_dim))
        q = ops.transpose(q, (0, 2, 1, 3))
        k = ops.reshape(self.k(x), (b, n, self.num_heads, self.head_dim))
        k = ops.transpose(k, (0, 2, 1, 3))
        v = ops.reshape(self.v(x), (b, n, self.num_heads, -1))
        v = ops.transpose(v, (0, 2, 1, 3))

        attn = self.bmm(q, ops.transpose(k, (0, 1, 3, 2))) * self.scale
        attn = self.softmax(attn)
        attn = self.attn_drop(attn)

        x = ops.transpose(self.bmm(attn, v), (0, 2, 1, 3))
        x = ops.reshape(x, (b, n, -1))
        x = self.proj(x)
        x = self.proj_drop(x)
        return x


class Block(nn.Cell):
    """Basic Block in TNT"""

    def __init__(self, outer_dim, inner_dim, outer_num_heads, inner_num_heads, num_words, mlp_ratio=4.,
                 qkv_bias=False, qk_scale=None, drop=0., attn_drop=0., drop_path=0., act_layer=nn.GELU,
                 norm_layer=nn.LayerNorm, se=0):
        super().__init__()
        self.has_inner = inner_dim > 0
        if self.has_inner:
            # Inner
            self.inner_norm1 = norm_layer((inner_dim,))
            self.inner_attn = Attention(
                inner_dim, inner_dim, num_heads=inner_num_heads, qkv_bias=qkv_bias,
                qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
            self.inner_norm2 = norm_layer((inner_dim,))
            self.inner_mlp = Mlp(in_features=inner_dim, hidden_features=int(inner_dim * mlp_ratio),
                                 out_features=inner_dim, act_layer=act_layer, drop=drop)
            self.proj_norm1 = norm_layer((num_words * inner_dim,))
            self.proj = nn.Dense(in_channels=num_words * inner_dim, out_channels=outer_dim, has_bias=False)
            self.proj_norm2 = norm_layer((outer_dim,))
        # Outer
        self.outer_norm1 = norm_layer((outer_dim,))
        self.outer_attn = Attention(
            outer_dim, outer_dim, num_heads=outer_num_heads, qkv_bias=qkv_bias,
            qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        self.drop_path = DropPath(drop_path)
        self.outer_norm2 = norm_layer((outer_dim,))
        self.outer_mlp = Mlp(in_features=outer_dim, hidden_features=int(outer_dim * mlp_ratio),
                             out_features=outer_dim, act_layer=act_layer, drop=drop)
        # SE
        self.se = se
        self.se_layer = 0
        if self.se > 0:
            self.se_layer = SE(outer_dim, 0.25)
        self.zeros = Tensor(np.zeros([1, 1, 1]), dtype=ms.float32)
        self.concat = ops.Concat(1)

    def construct(self, inner_tokens, outer_tokens):
        if self.has_inner:
            inner_tokens = inner_tokens + self.drop_path(self.inner_attn(self.inner_norm1(inner_tokens)))  # b*n, k*k, c
            inner_tokens = inner_tokens + self.drop_path(self.inner_mlp(self.inner_norm2(inner_tokens)))  # b*n, k*k, c
            b, n, c = outer_tokens.shape
            zeros = ops.tile(self.zeros, (b, 1, c))
            proj = self.proj_norm2(self.proj(self.proj_norm1(ops.reshape(inner_tokens, (b, n - 1, -1,)))))
            proj = ops.cast(proj, ms.float32)
            proj = self.concat((zeros, proj))
            outer_tokens = outer_tokens + proj  # b, n, c
        if self.se > 0:
            outer_tokens = outer_tokens + self.drop_path(self.outer_attn(self.outer_norm1(outer_tokens)))
            tmp_ = self.outer_mlp(self.outer_norm2(outer_tokens))
            outer_tokens = outer_tokens + self.drop_path(tmp_ + self.se_layer(tmp_))
        else:
            outer_tokens = outer_tokens + self.drop_path(self.outer_attn(self.outer_norm1(outer_tokens)))
            outer_tokens = outer_tokens + self.drop_path(self.outer_mlp(self.outer_norm2(outer_tokens)))
        return inner_tokens, outer_tokens


class PatchEmbed(nn.Cell):
    """Image to Visual Word Embedding"""

    def __init__(self, img_size=224, patch_size=16, in_chans=3, outer_dim=768, inner_dim=24, inner_stride=4):
        super().__init__()
        img_size = img_size if isinstance(img_size, tuple) else (img_size,) * 2
        patch_size = patch_size if isinstance(patch_size, tuple) else (patch_size,) * 2
        num_patches = (img_size[1] // patch_size[1]) * (img_size[0] // patch_size[0])
        self.img_size = img_size
        self.patch_size = patch_size
        self.num_patches = num_patches
        self.inner_dim = inner_dim
        self.num_words = math.ceil(patch_size[0] / inner_stride) * math.ceil(patch_size[1] / inner_stride)

        self.unfold = UnfoldKernelEqPatch(kernel_size=patch_size, strides=patch_size)
        self.proj = nn.Conv2d(in_channels=in_chans, out_channels=inner_dim, kernel_size=7, stride=inner_stride,
                              pad_mode='pad', padding=3, has_bias=False)

    def construct(self, x):
        b = x.shape[0]
        x = self.unfold(x)  # b, Ck2, n
        x = self.proj(x)  # b*n, C, 8, 8
        x = ops.reshape(x, (b * self.num_patches, self.inner_dim, -1,))  # b*n, 8*8, n
        x = ops.transpose(x, (0, 2, 1))
        return x


class TNT(nn.Cell):
    r"""TNT model class, based on
    `"Transformer in Transformer" <https://arxiv.org/abs/2103.00112>`_
    """

    def __init__(self, img_size=224, patch_size=16, in_chans=3, num_classes=1000, outer_dim=768, inner_dim=48,
                 depth=12, outer_num_heads=12, inner_num_heads=4, mlp_ratio=4., qkv_bias=False, qk_scale=None,
                 drop_rate=0., attn_drop_rate=0., drop_path_rate=0., norm_layer=nn.LayerNorm, inner_stride=4, se=0):
        super().__init__()
        self.num_classes = num_classes
        self.outer_dim = outer_dim

        self.patch_embed = PatchEmbed(
            img_size=img_size, patch_size=patch_size, in_chans=in_chans, outer_dim=outer_dim,
            inner_dim=inner_dim, inner_stride=inner_stride)
        self.num_patches = num_patches = self.patch_embed.num_patches
        num_words = self.patch_embed.num_words

        self.proj_norm1 = norm_layer((num_words * inner_dim,))
        self.proj = nn.Dense(in_channels=num_words * inner_dim, out_channels=outer_dim, has_bias=False)
        self.proj_norm2_tnt = norm_layer((outer_dim,))

        self.cls_token = Parameter(Tensor(trunc_array([1, 1, outer_dim]), dtype=ms.float32), name="cls_token",
                                   requires_grad=True)
        self.outer_pos = Parameter(Tensor(trunc_array([1, num_patches + 1, outer_dim]), dtype=ms.float32),
                                   name="outer_pos")
        self.inner_pos = Parameter(Tensor(trunc_array([1, num_words, inner_dim]), dtype=ms.float32))
        self.pos_drop = nn.Dropout(keep_prob=1.0 - drop_rate)

        dpr = [x for x in np.linspace(0, drop_path_rate, depth)]  # stochastic depth decay rule
        vanilla_idxs = []
        blocks = []
        for i in range(depth):
            if i in vanilla_idxs:
                blocks.append(Block(
                    outer_dim=outer_dim, inner_dim=-1, outer_num_heads=outer_num_heads, inner_num_heads=inner_num_heads,
                    num_words=num_words, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale, drop=drop_rate,
                    attn_drop=attn_drop_rate, drop_path=dpr[i], norm_layer=norm_layer, se=se))
            else:
                blocks.append(Block(
                    outer_dim=outer_dim, inner_dim=inner_dim, outer_num_heads=outer_num_heads,
                    inner_num_heads=inner_num_heads,
                    num_words=num_words, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale, drop=drop_rate,
                    attn_drop=attn_drop_rate, drop_path=dpr[i], norm_layer=norm_layer, se=se))
        self.blocks = nn.CellList(blocks)
        self.norm = norm_layer((outer_dim,))

        # NOTE as per official impl, we could have a pre-logits representation dense layer + tanh here
        # self.repr = nn.Linear(outer_dim, representation_size)
        # self.repr_act = nn.Tanh()

        # Classifier head
        mask = np.zeros([1, num_patches + 1, 1])
        mask[:, 0] = 1
        self.mask = Tensor(mask, dtype=ms.float32)
        self.head = nn.Dense(in_channels=outer_dim, out_channels=num_classes, has_bias=False)
        self.concat = ops.Concat(1)
        self._init_weights()

    def _init_weights(self):
        for _, cell in self.cells_and_names():
            if isinstance(cell, nn.Dense):
                cell.weight.set_data(
                    init.initializer(init.TruncatedNormal(sigma=0.02), cell.weight.shape, cell.weight.dtype))
                if isinstance(cell, nn.Dense) and cell.bias is not None:
                    cell.bias.set_data(init.initializer(init.Zero(), cell.bias.shape, cell.bias.dtype))
            elif isinstance(cell, nn.LayerNorm):
                cell.gamma.set_data(init.initializer(init.One(), cell.gamma.shape, cell.gamma.dtype))
                cell.beta.set_data(init.initializer(init.Zero(), cell.beta.shape, cell.beta.dtype))

    def forward_features(self, x):
        b = x.shape[0]
        inner_tokens = self.patch_embed(x) + self.inner_pos  # b*N, 8*8, C

        outer_tokens = self.proj_norm2_tnt(
            self.proj(self.proj_norm1(ops.reshape(inner_tokens, (b, self.num_patches, -1,)))))
        outer_tokens = ops.cast(outer_tokens, ms.float32)
        outer_tokens = self.concat((ops.tile(self.cls_token, (b, 1, 1)), outer_tokens))

        outer_tokens = outer_tokens + self.outer_pos
        outer_tokens = self.pos_drop(outer_tokens)

        for blk in self.blocks:
            inner_tokens, outer_tokens = blk(inner_tokens, outer_tokens)

        outer_tokens = self.norm(outer_tokens)  # [batch_size, num_patch+1, outer_dim)
        return outer_tokens[:, 0]

    def construct(self, x):
        x = self.forward_features(x)
        x = self.head(x)
        return x


def tnt_s_patch16_224(**kwargs):
    patch_size = 16
    inner_stride = 4
    outer_dim = 384
    inner_dim = 24
    outer_num_heads = 6
    inner_num_heads = 4
    outer_dim = make_divisible(outer_dim, outer_num_heads)
    inner_dim = make_divisible(inner_dim, inner_num_heads)
    model = TNT(img_size=224, patch_size=patch_size, outer_dim=outer_dim, inner_dim=inner_dim, depth=12,
                outer_num_heads=outer_num_heads, inner_num_heads=inner_num_heads, qkv_bias=False,
                inner_stride=inner_stride, **kwargs)
    return model


def tnt_b_patch16_224(**kwargs):
    patch_size = 16
    inner_stride = 4
    outer_dim = 640
    inner_dim = 40
    outer_num_heads = 10
    inner_num_heads = 4
    outer_dim = make_divisible(outer_dim, outer_num_heads)
    inner_dim = make_divisible(inner_dim, inner_num_heads)
    model = TNT(img_size=224, patch_size=patch_size, outer_dim=outer_dim, inner_dim=inner_dim, depth=12,
                outer_num_heads=outer_num_heads, inner_num_heads=inner_num_heads, qkv_bias=False,
                inner_stride=inner_stride, **kwargs)
    return model


if __name__ == "__main__":
    import numpy as np
    import mindspore
    from mindspore import Tensor
    from mindspore import context

    context.set_context(mode=mindspore.PYNATIVE_MODE)

    net = tnt_s_patch16_224()
    dummy_input = Tensor(np.random.rand(8, 3, 224, 224), dtype=mindspore.float32)
    y = net(dummy_input)
    print("Shape of out :", y.shape)
