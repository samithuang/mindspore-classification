from typing import Union, Optional

from mindspore import nn
from mindspore import Tensor, dtype
from mindspore import ops
import mindspore.nn.probability.distribution as msd




class DropPath(nn.Cell):
    """
    Drop paths (Stochastic Depth) per sample  (when applied in main path of residual blocks).
    """

    def __init__(self, drop_rate=0., seed=0):
        super(DropPath, self).__init__()
        self.keep_prob = 1 - drop_rate
        self.rand = ops.UniformReal(seed=seed)
        self.shape = ops.Shape()
        self.floor = ops.Floor()
        self.div = ops.Div()

    def construct(self, x):
        if self.keep_prob == 1.0 or not self.training:
            return x
        x_shape = self.shape(x)
        random_tensor = self.rand((x_shape[0], 1, 1))
        random_tensor = random_tensor + self.keep_prob
        random_tensor = self.floor(random_tensor)
        x = self.div(x, self.keep_prob)
        x = x * random_tensor

        return x


class StochasticDepthDrop(nn.Cell):
    """
    DropConnect function.

    Args:
        keep_prob (int): Drop rate of the MBConv Block. Default: 0.

    Return:
        Tensor

    Example:
        >>> import mindspore
        >>> from mindspore import Tensor
        >>> x = Tensor(((20, 16), (50, 50)), mindspore.float32)
        >>> StochasticDepthDrop(0.8)(x)
    """
    def __init__(self,
                 keep_prob: Optional[float] = None
                 ):
        super(StochasticDepthDrop, self).__init__()
        self.drop_rate = keep_prob
        self.bernoulli = msd.Bernoulli(probs=self.drop_rate, dtype=dtype.int32)

    def construct(self, x: Tensor):
        if not self.training or self.drop_rate == 0.:
            return x
        return x * self.bernoulli.sample((x.shape[0],) + (1,) * (x.ndim-1))
