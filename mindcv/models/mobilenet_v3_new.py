# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""""""

import math
from typing import Optional, List

import mindspore.nn as nn
import mindspore.ops as ops
import mindspore.common.initializer as init
from mindspore import Tensor

from .layers.conv_norm_act import Conv2dNormActivation
from .layers.blocks import SqueezeExcite
from .layers.pooling import GlobalAvgPooling
from .utils import load_pretrained, make_divisible
from .registry import register_model


def _cfg(url='', **kwargs):
    return {
        'url': url,
        'num_classes': 1000,
        'first_conv': '', 'classifier': '',
        **kwargs
    }


default_cfgs = {
    'mobilenet_v3_small_1.0': _cfg(url=''),
    'mobilenet_v3_large_1.0': _cfg(url=''),
    'mobilenet_v3_small_0.75': _cfg(url=''),
    'mobilenet_v3_large_0.75': _cfg(url='')
}


class ResUnit(nn.Cell):

    def __init__(self,
                 num_in: int,
                 num_mid: int,
                 num_out: int,
                 kernel_size: int,
                 norm: nn.Cell,
                 activation: str,
                 stride: int = 1,
                 use_se: bool = False) -> None:
        super(ResUnit, self).__init__()
        self.use_se = use_se
        self.use_short_cut_conv = num_in == num_out and stride == 1
        self.use_hs = activation == 'hswish'
        self.activation = nn.HSwish if self.use_hs else nn.ReLU

        layers = []

        # Expand.
        if num_in != num_mid:
            layers.append(
                Conv2dNormActivation(num_in, num_mid, kernel_size=1, norm=norm, activation=self.activation)
            )

        # DepthWise.
        layers.append(
            Conv2dNormActivation(num_mid, num_mid, kernel_size=kernel_size, stride=stride, groups=num_mid, norm=norm,
                                 activation=self.activation)
        )
        if use_se:
            squeeze_channel = make_divisible(num_mid // 4, 8)
            layers.append(
                SqueezeExcite(num_mid, squeeze_channel, nn.ReLU, nn.HSigmoid)
            )

        # Project.
        layers.append(
            Conv2dNormActivation(num_mid, num_out, kernel_size=1, norm=norm, activation=None)
        )

        self.block = nn.SequentialCell(layers)
        self.add = ops.Add()

    def construct(self, x: Tensor) -> Tensor:
        out = self.block(x)

        if self.use_short_cut_conv:
            out = self.add(out, x)

        return out


class MobileNetV3(nn.Cell):

    def __init__(self,
                 model_cfgs: List,
                 last_channel: int,
                 in_channels: int = 3,
                 multiplier: float = 1.0,
                 norm: Optional[nn.Cell] = None,
                 round_nearest: int = 8,
                 num_classes: int = 1000,
                 drop_rate: float = 0.2
                 ) -> None:
        super(MobileNetV3, self).__init__()

        if not norm:
            norm = nn.BatchNorm2d

        self.inplanes = 16
        layers = []

        # Building first layer.
        first_conv_in_channel = in_channels
        first_conv_out_channel = make_divisible(self.inplanes * multiplier, round_nearest)
        layers.append(
            Conv2dNormActivation(
                first_conv_in_channel,
                first_conv_out_channel,
                kernel_size=3,
                stride=2,
                norm=norm,
                activation=nn.HSwish
            )
        )

        # Building inverted residual blocks.
        for layer_cfg in model_cfgs:
            layers.append(self._make_layer(kernel_size=layer_cfg[0],
                                           exp_ch=make_divisible(multiplier * layer_cfg[1], round_nearest),
                                           out_channel=make_divisible(multiplier * layer_cfg[2], round_nearest),
                                           use_se=layer_cfg[3],
                                           activation=layer_cfg[4],
                                           stride=layer_cfg[5],
                                           norm=norm
                                           )
                          )

        lastconv_input_channel = make_divisible(multiplier * model_cfgs[-1][2], round_nearest)
        lastconv_output_channel = lastconv_input_channel * 6

        # Building last several layers.
        layers.append(
            Conv2dNormActivation(
                lastconv_input_channel,
                lastconv_output_channel,
                kernel_size=1,
                norm=norm,
                activation=nn.HSwish
            )
        )

        self.features = nn.SequentialCell(layers)

        self.pool = GlobalAvgPooling(keep_dim=True)
        self.num_features = last_channel
        self.conv_head = nn.SequentialCell([
            nn.Conv2d(in_channels=lastconv_output_channel,
                      out_channels=self.num_features,
                      kernel_size=1,
                      stride=1),
            nn.HSwish(),
        ])
        self.dropout = nn.Dropout(keep_prob=1 - drop_rate)
        self.classifier = nn.Conv2d(in_channels=self.num_features,
                                    out_channels=num_classes,
                                    kernel_size=1,
                                    has_bias=True)
        self.squeeze = ops.Squeeze(axis=(2, 3))
        self._initialize_weights()

    def _initialize_weights(self) -> None:
        for _, cell in self.cells_and_names():
            if isinstance(cell, nn.Conv2d):
                n = cell.kernel_size[0] * cell.kernel_size[1] * cell.out_channels
                cell.weight.set_data(
                    init.initializer(init.Normal(sigma=math.sqrt(2. / n), mean=0.0),
                                     cell.weight.shape,
                                     cell.weight.dtype
                                     ),
                )
                if cell.bias is not None:
                    cell.bias.set_data(
                        init.initializer('zeros', cell.bias.shape, cell.bias.dtype)
                    )
            elif isinstance(cell, nn.BatchNorm2d):
                cell.gamma.set_data(
                    init.initializer('ones', cell.gamma.shape, cell.gamma.dtype)
                )
                cell.beta.set_data(
                    init.initializer('zeros', cell.beta.shape, cell.beta.dtype)
                )
            elif isinstance(cell, nn.Dense):
                cell.weight.set_data(
                    init.initializer(init.Normal(sigma=0.01, mean=0.0),
                                     cell.weight.shape,
                                     cell.weight.dtype
                                     )
                )
                if cell.bias is not None:
                    cell.bias.set_data(
                        init.initializer('zeros', cell.bias.shape, cell.bias.dtype)
                    )

    def forward_features(self, x: Tensor) -> Tensor:
        x = self.features(x)
        return x

    def forward_head(self, x: Tensor) -> Tensor:
        x = self.pool(x)
        x = self.conv_head(x)
        x = self.dropout(x)
        x = self.classifier(x)
        x = self.squeeze(x)
        return x

    def construct(self, x: Tensor) -> Tensor:
        x = self.forward_features(x)
        x = self.forward_head(x)
        return x

    def _make_layer(self,
                    kernel_size: int,
                    exp_ch: int,
                    out_channel: int,
                    use_se: bool,
                    activation: str,
                    norm: nn.Cell,
                    stride: int = 1
                    ) -> ResUnit:
        """Block layers."""
        layer = ResUnit(self.inplanes, exp_ch, out_channel,
                        kernel_size=kernel_size, stride=stride, activation=activation, use_se=use_se, norm=norm)
        self.inplanes = out_channel

        return layer


model_cfgs = {
    "large": [
        [3, 16, 16, False, 'relu', 1],
        [3, 64, 24, False, 'relu', 2],
        [3, 72, 24, False, 'relu', 1],
        [5, 72, 40, True, 'relu', 2],
        [5, 120, 40, True, 'relu', 1],
        [5, 120, 40, True, 'relu', 1],
        [3, 240, 80, False, 'hswish', 2],
        [3, 200, 80, False, 'hswish', 1],
        [3, 184, 80, False, 'hswish', 1],
        [3, 184, 80, False, 'hswish', 1],
        [3, 480, 112, True, 'hswish', 1],
        [3, 672, 112, True, 'hswish', 1],
        [5, 672, 160, True, 'hswish', 2],
        [5, 960, 160, True, 'hswish', 1],
        [5, 960, 160, True, 'hswish', 1]
    ],
    "small": [
        [3, 16, 16, True, 'relu', 2],
        [3, 72, 24, False, 'relu', 2],
        [3, 88, 24, False, 'relu', 1],
        [5, 96, 40, True, 'hswish', 2],
        [5, 240, 40, True, 'hswish', 1],
        [5, 240, 40, True, 'hswish', 1],
        [5, 120, 48, True, 'hswish', 1],
        [5, 144, 48, True, 'hswish', 1],
        [5, 288, 96, True, 'hswish', 2],
        [5, 576, 96, True, 'hswish', 1],
        [5, 576, 96, True, 'hswish', 1]]
}


@register_model
def mobilenet_v3_small_100(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> MobileNetV3:
    default_cfg = default_cfgs['mobilenet_v3_small_1.0']
    model = MobileNetV3(model_cfgs=model_cfgs['small'],
                        last_channel=1280,
                        in_channels=in_channels,
                        multiplier=1.0,
                        num_classes=num_classes,
                        **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def mobilenet_v3_large_100(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> MobileNetV3:
    default_cfg = default_cfgs['mobilenet_v3_large_1.0']
    model = MobileNetV3(model_cfgs=model_cfgs['large'],
                        last_channel=1280,
                        in_channels=in_channels,
                        multiplier=1.0,
                        num_classes=num_classes,
                        **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def mobilenet_v3_small_075(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> MobileNetV3:
    default_cfg = default_cfgs['mobilenet_v3_small_0.75']
    model = MobileNetV3(model_cfgs=model_cfgs['small'],
                        last_channel=1280,
                        in_channels=in_channels,
                        multiplier=0.75,
                        num_classes=num_classes,
                        **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model


@register_model
def mobilenet_v3_large_075(pretrained: bool = False, num_classes: int = 1000, in_channels=3, **kwargs) -> MobileNetV3:
    default_cfg = default_cfgs['mobilenet_v3_large_0.75']
    model = MobileNetV3(model_cfgs=model_cfgs['large'],
                        last_channel=1280,
                        in_channels=in_channels,
                        multiplier=0.75,
                        num_classes=num_classes,
                        **kwargs)

    if pretrained:
        load_pretrained(model, default_cfg, num_classes=num_classes, in_channels=in_channels)

    return model
